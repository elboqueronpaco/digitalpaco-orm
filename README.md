# Digital paco

Cuando instales el orm creer un archivo `.env` 

## Creación del archivo `.env` 

Cuando cree el dicho archivo tienes que cambiar las variable de entorno por la tuyas de tu base de dato:

```

BD_SERVIDOR =     '127.0.0.1' 
DB_NOMBRE =       'nombre_bd' //nombre de la base de dato
DB_PUERTO =       3606
DB_USUARIO =      'nombre_usuario' //nombre usuario base de dato
DB_CLAVE =        'contraseña' //contraseña de usuario de  base de dato
DB_CHARSET =      'utf8'
DB_CONDUCTOR=     'mysql' // por defecto esta en mysql

```