<?php
require_once __DIR__ . '/vendor/autoload.php';
use Dotenv\Dotenv;

/**
 * Carga las variables de entorno
 */
$dotenv = Dotenv::create(__DIR__);
$dotenv->load();
$dotenv->required(
  'BD_SERVIDOR',
  'DB_NOMBRE',
  'DB_PUERTO',
  'DB_USUARIO',
  'DB_CLAVE',
  'DB_CONDUCTOR',
  'DB_CHARSET'
);
define('BASE_DATOS', [
  'servidor'    => $_ENV['BD_SERVIDOR'],
  'puerto'      => $_ENV['DB_PUERTO'],
  'nombre'      => $_ENV['DB_NOMBRE'],
  'usuario'     => $_ENV['DB_USUARIO'],
  'clave'       => $_ENV['DB_CLAVE'],
  'charset'     => $_ENV['DB_CHARSET'],
  'conductor'      => $_ENV['DB_CONDUCTOR']
]);
var_dump(BASE_DATOS);
