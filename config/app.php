<?php

require_once __DIR__ . '/../vendor/autoload.php';

/**
 * Cargar configuración de la conexión
 */
define('BASE_DATOS', [
  'servidor'    => $_ENV['DB_SERVIDOR'],
  'puerto'      => $_ENV['DB_PUERTO'],
  'nombre'      => $_ENV['DB_NOMBRE'],
  'usuario'     => $_ENV['DB_USUARIO'],
  'clave'       => $_ENV['DB_CLAVE'],
  'charset'     => $_ENV['DB_CHARSET'],
  'conductor'      => $_ENV['DB_CONDUCTOR']
]);
var_dump(BASE_DATOS);
